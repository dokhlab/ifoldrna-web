#!/bin/env/python
#
# by Benfeard Williams
# June 2016
#
# This script converts RNA dsecondary structure in dot bracket
# format to a base pair format ready for 3bead DMD simulations
#
# usage: python dotBracket2bpList.py secStruc
#
import sys
from operator import itemgetter
from itertools import chain as chaintool

# check for proper usage of script
if len(sys.argv) != 2:
	print "usage: python parse_motifs.py secStruc"
	sys.exit()

dotBracket = sys.argv[-1]
length = len(dotBracket)

# find base pairs from the secondary structure
stack = [] ## contains indices of left brackets
stack_square = [] ## contains indices of left square brackets
stack_curly = []
stack_arrow = []
basePairs = []
for index in range(length):
	symbol = dotBracket[index]
	if symbol == "(":
		stack.append(index)
	elif symbol == "[":
		stack_square.append(index)
	elif symbol == "{":
		stack_curly.append(index)
	elif symbol == "<":
		stack_arrow.append(index)
	elif symbol == ")":
		if len(stack) == 0:
			sys.stderr.write("Error found with base pairs!\n")
		basePairs.append((stack.pop(),index))
	elif symbol == "]":
		if len(stack_square) == 0:
			sys.stderr.write("Error found with pseudoknot!\n")
		basePairs.append((stack_square.pop(),index))
	elif symbol == "}":
		basePairs.append((stack_curly.pop(),index))
	elif symbol == ">":
		basePairs.append((stack_arrow.pop(),index))
basePairs = sorted(basePairs,key=itemgetter(0))
bpList = list(chaintool.from_iterable(basePairs))

if len(stack) > 0:
	sys.stderr.write("Error found with base pairs!\n")
numBP = len(basePairs)

bpDict = dict(basePairs) ## base pairs as dictionary

for key in bpDict.keys():
	print "A"+str((int(key)+1)), "A"+str((bpDict[key]+1))
