<?php

require_once(__DIR__.'/mail_utils.php');
require_once(__DIR__.'/config.php');

$id = $argv[1];
$email = '';

function set_inputs() {
    global $email, $id, $daemon_path;

    echo "Fetch Inputs of $id...\n";

    $conn = database_connect();

    $query = "select * from ifoldrna_tasks where id='$id'";

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    echo json_encode($row)."\n";

    if (!empty($row)) {
        // Create execution directories and input files
        $input_path = "$daemon_path/exec/$id/input";
        if (!file_exists($input_path)) {
            echo "Making directory: $input_path...\n";
            mkdir($input_path, 0777, true);
        }

        foreach ($row as $k => $v) {
            if ($k == 'files') {
                $files = json_decode($v, true);
                foreach ($files as $file_name => $file_content) {
                    file_put_contents("$input_path/$file_name", $file_content);
                }
            } else {
                file_put_contents("$input_path/$k", $v);
            }
        }

        $userid = $row['userid'];
        $query = "select id, email from users where id='$userid'";

        $stmt = $conn->prepare($query); 
        $stmt->execute(); 
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!empty($row)) {
            $email = $row['email'];
        }

        return true;
    }

    $conn = null;
}

function send_result_mail($success) {
    global $id, $email;
    $host = "https://dokhlab.med.psu.edu";

    if (!empty($email)) {
      if ($success) {
        $subject = "iFoldRNA Task Finished";
        $message  = "Dear User,\n\n";
        $message .= "Thank you for using iFoldRNA! Your Task $id has been finished. Please click this link ($host/ifoldrna/#/task/$id) for details!\n\n";
        $message .= "Sincerely,\n";
        $message .= "iFoldRNA Team";
      } else {
        $subject = "iFoldRNA Task Failed";
        $message  = "Dear User,\n\n";
        $message .= "Thank you for using iFoldRNA! Your Task $id has failed. Please click this link ($host/ifoldrna/#/task/$id) for details!\n\n";
        $message .= "Sincerely,\n";
        $message .= "iFoldRNA Team";
      }
      send_mail($email , $subject , $message);
    }
}


function ifoldrna() {
//    global $id, $email, $daemon_path, $file_store;
    global $id, $email, $daemon_path;

    database_update($id, ["status"=>"predicting"], ["tprocess"=>"CURRENT_TIMESTAMP()"]);

    $par           = json_decode(file_get_contents("$daemon_path/exec/$id/input/par"), true);

    foreach ($par as $par_key => $par_value) {
      if (is_array($par_value)) {
        $par_value = implode("\n", $par_value);
      }
      file_put_contents("$daemon_path/exec/$id/input/$par_key", $par_value);
    }

    $seq = $par['seq'];
    $ss  = $par['ss'];

    // Set structure file
    $log_file        = "$daemon_path/exec/$id/log.txt";

    // Set dmd.sim
    echo "Set dmd.sim...\n";
    $dmdDir = "$daemon_path/exec/$id/dmd.sim";
    $cmd = "mkdir -p $dmdDir; cp $daemon_path/scripts/* $dmdDir";
    shell_exec($cmd);

//    // Set seq
//    echo "Set seq...\n";
//    $cmd = "(echo $seq | awk '{ print toupper($0) }' | fold -w1 | perl -lane 's/T/U/g;print'; echo END) >$daemon_path/exec/$id/dmd.sim/seq";
//    shell_exec($cmd);
//
//    // Set bp.const
//    echo "Set bp.const...\n";
//    $cmd = "python $daemon_path/bin/dotBracket2bpList.py '$ss' >$daemon_path/exec/$id/dmd.sim/bp.const";
//    shell_exec($cmd);

    // iFoldRNA
    echo "iFoldRNA...\n";
    $cmd = "cd $daemon_path/exec/$id/dmd.sim; bash run.sh >$log_file 2>&1";
    shell_exec($cmd);

    // Get the number of Clusters
    $n = shell_exec("ls $daemon_path/exec/$id/dmd.sim/cluster.*.pdb | wc -l");
    echo "$n Clusters...\n";

    $results = ['clusters'=>$n];

    if ($n == 0) {
      database_update($id, ["status"=>"failed", "results"=>json_encode($results)], ["tfinish"=>"CURRENT_TIMESTAMP()"]);
      send_result_mail(false);
    } else {
      database_update($id, ["status"=>"finished", "results"=>json_encode($results)], ["tfinish"=>"CURRENT_TIMESTAMP()"]);
      send_result_mail(true);
    }

}

set_inputs();
ifoldrna();

