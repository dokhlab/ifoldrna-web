<?php

require_once(__DIR__.'/config.php');

#echo "Distributing...\n";

$conn = database_connect();

// Submit ifoldrna tasks
$query = "select * from ifoldrna_tasks where status<>'finished' and status<>'failed'";

$stmt = $conn->prepare($query); 
$stmt->execute(); 
$rows = $stmt->fetchAll();
$cnt = count($rows);

for ($i = $cnt - 1; $i >= 0; $i--) {
    $v = $rows[$i];

#foreach ($rows as $k => $v) {
    $id = $v['id'];
#    echo "Handle ID: $id\n";
    $n = intval(shell_exec("ps aux | grep 'php ifoldrna.php $id' | grep -v grep | wc -l"));
#    echo "$n threads are used to process Task $id.\n";
    $num_total_runnings = intval(shell_exec("ps aux | grep 'php ifoldrna.php' | grep -v grep | wc -l"));
#    echo "$num_total_runnings threads are totally used.\n";
    if ($num_total_runnings < $max_tasks && $n == 0) {
        echo "Submit iFoldRNA Task $id.\n";
        pclose(popen("php ifoldrna.php $id >".__DIR__."/../logs/$id.log 2>&1 &", 'r'));
    }
}

// Stop tasks
$query = "select * from ifoldrna_tasks where status='stopping'";

$stmt = $conn->prepare($query);                        
$stmt->execute();   
$rows = $stmt->fetchAll();

foreach ($rows as $k => $v) {
    $id = $v['id'];
#    echo "Task $id are labeled to be stopped.\n";
    $n = intval(shell_exec("ps aux | grep 'php ifoldrna.php $id' | grep -v grep | wc -l"));
#    echo "$n threads are used to process Task $id.\n";
    if ($n != 0) {
      echo "Stopping $id\n";
      shell_exec('kill $(ps aux | grep -v grep | grep "php ifoldrna.php '.$id.'" | perl -lane \'print $F[1]\')');
      $query = "update ifoldrna_tasks set status='failed' where id='$id'";
      $conn->exec($query);
    }
}

$conn = null;

