<?php

require_once(__DIR__.'/mail_utils.php');

$config      = json_decode(file_get_contents(__DIR__.'/../../config.json'), true);

$dbhost      = $config['db']['host'];
$dbuser      = $config['db']['user'];
$dbpass      = $config['db']['password'];
$dbname      = $config['db']['database'];

$file_store  = "/mnt/dokhlab";

// $ftphost      = $config['ftp']['host'];
// $ftpuser      = $config['ftp']['user'];
// $ftppass      = $config['ftp']['password'];
// $ftpfolder      = $config['ftp']['folder'];

$daemon_path = __DIR__."/..";
#$babel_path  = $config['babel_path'];
$host        = $config['host'];
#$pymol_path  = $config['pymol_path'];

$max_tasks   = 3;

function database_connect() {
    global $dbhost, $dbname, $dbuser, $dbpass;

    $conn = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;
}

function database_update($id, $m1, $m2 = []) {
    $conn = database_connect();

    $query = "update ifoldrna_tasks set ";
    $tmp = [];
    foreach ($m1 as $k => $v) {
        $q = $conn->quote($v);
        $tmp[] = "$k=$q";
    }
    foreach ($m2 as $k => $v) {
        $q = $v;
        $tmp[] = "$k=$q";
    }
    $query .= implode(',', $tmp);
    $query .= " where id=$id";

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $conn = null;
}

function save_file($file) {
    $ext = pathinfo($file, PATHINFO_EXTENSION);
    $name = tempnam(__DIR__."/../blobs", '').".$ext";
    $cmd = "cp $file $name; chmod 777 $name";
    system($cmd);
    $name = pathinfo($name, PATHINFO_BASENAME);
    return $name;
}


