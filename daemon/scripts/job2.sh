#!/bin/bash -l

. /etc/profile.d/modules.sh

module load hrp
module load anaconda

init-hrp .

pdb=$(cd ../input;pwd)/pdb

cp $pdb ~input.pdb
cp ~input.pdb input.pdb
perl -i -lane 's/GUA/  G/g;s/CYT/  C/g;s/URI/  U/g;s/ADE/  A/g;if(not / H/){print}' input.pdb

touch constraints.txt

pdb2txt.linux input.pdb 500 >init.txt

N=$(n=$(head -n4 init.txt | tail -n1);echo $(((n+1)/3)))

mpirun --oversubscribe -np 8 dmd-rx-mc.linux -i start -r start-8.rex -L

cat *HRP*.echo >all.echo
perl -i -lane 'print if not /#/' all.echo

python fel.py all.echo 'Energy' 3 'Gyration Radius' 4 fel.png

average-reactivity.pl MBox.hrp $N >ave.hrp

python pdb2seq.py input.pdb seq

# Clustering
analyze.pl input.pdb seq ave.hrp $N 8 clustering

perl -lane 'if(/Centroid: (\S+)\s*$/){print $1}' clustering | head -n5 >in

# All-atom
n=1
for i in $(cat in); do
  $HRP_HOME/iFoldRNA/MEDUSArecon_3BeadRNA.exe $HRP_HOME/iFoldRNA/medusa/parameter/ $i >cluster.${n}.pdb
  n=$((n+1))
done

tar cvzf clusters.tar.gz cluster*.pdb fel.png

