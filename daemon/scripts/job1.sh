#!/bin/bash -l

. /etc/profile.d/modules.sh

module load hrp

init-hrp .

# set simulation time
if [ -f ../input/simTime ]; then
  simTime=$(cat ../input/simTime)
  perl -i -lane 'if(/^MAX_TIME\s/){print "MAX_TIME '$simTime'"}else{print $_}' start
fi

# set temperatures
if [ -f ../input/repT ]; then
  n=0
  (cat ../input/repT; echo) | while read line; do
    if [ ! -z $line ]; then
      n=$((n+1))
      perl -i -lane 'if(/^REPLICA\s/){$n++;if($n=='$n'){print "REPLICA '$((n-1))' '$line'"}else{print $_}}else{print $_}' start-8.rex
    fi
  done
fi

# set remd interval
if [ -f ../input/repInt ]; then
  repInt=$(cat ../input/repInt)
  perl -i -lane 'if(/^RX_DT\s/){print "RX_DT '$repInt'"}else{print $_}' start-8.rex
fi

# set heat exchange coefficient
if [ -f ../input/heatExchCoeff ]; then
  heatExchCoeff=$(cat ../input/heatExchCoeff)
  perl -i -lane 'if(/^HEAT_X_C\s/){print "HEAT_X_C '${heatExchCoeff}'"}else{print $_}' start
fi

# set sequence
seq=$(cat ../input/seq)
(echo $seq | awk '{ print toupper($0) }' | fold -w1 | perl -lane 's/T/U/g;print'; echo END) >seq

# set secondary structure
if [ -f ../input/ss ]; then
  ss=$(cat ../input/ss)
else
  ss=""
fi
python dotBracket2bpList.py "$ss" >bp.const

# set constraints
touch constraints.txt

N=$(wc -l seq | perl -lane 'print $F[0]-1')
echo "Number of Residue: $N"

echo "Generate the 'pre.txt' file"
nseq_cons2txt.linux seq bp.const 1000 >pre.txt # 1000 is the boxsize

echo "Run DMD"
dmd.linux -i relax

echo "Convert the movie to pdb file"
# >>> mov2pdbs.linux movie seq outPDB [initFrame nFrame dN]
nseq_movie2pdb.linux relax.mov seq relaxed.pdb 1000 1 1


################ Step 2 Second round of MD

echo "Run DMD"
dmd.linux -i relax-loop -L

echo "Convert movie to pdb"
nseq_movie2pdb.linux relax-L.mov seq init.pdb  100 1 1


################ Step 3 REMD

echo "Prepare for REMD"
prepare.pl init.pdb MBox.hrp bp.const 500 init1.txt

echo "Add constraints"
perl -lane 'print if $n==0;$n=1 if /CONSTRAINTS/' init1.txt >init2.txt
cat constraints.txt >>init2.txt
perl -lane 'print if $n==1;$n=1 if /CONSTRAINTS/' init1.txt >>init2.txt

python check-constraints.py init2.txt init.txt

mpirun --oversubscribe -np 8 dmd-rx-mc.linux -i start -r start-8.rex -L

# Prepare the average reaction file
average-reactivity.pl MBox.hrp $N >ave.hrp

# Get native pdb
$HRP_HOME/iFoldRNA/MEDUSArecon_3BeadRNA.exe $HRP_HOME/iFoldRNA/medusa/parameter/ relaxed.pdb >allatom.pdb

# Clustering
analyze.pl allatom.pdb seq ave.hrp $N 8 clustering

perl -lane 'if(/Centroid: (\S+)\s*$/){print $1}' clustering | head -n5 >in

# All-atom
n=1
for i in $(cat in); do
  $HRP_HOME/iFoldRNA/MEDUSArecon_3BeadRNA.exe $HRP_HOME/iFoldRNA/medusa/parameter/ $i >cluster.${n}.pdb
  n=$((n+1))
done

tar cvzf clusters.tar.gz cluster*.pdb

