<?php

require_once(__DIR__.'/utils.php');

user_do(function ($conn) {
  $id = $_GET['id'];
  $userid = $_SESSION['user']['id'];
  $userlevel = $_SESSION['user']['level'];

  $query = "select id, userid, status from ifoldrna_tasks where id='$id'";
  $stmt = $conn->prepare($query); 
  $stmt->execute(); 
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  if ($userid == $row['userid'] || $userlevel == '10') {
    if ($row['status'] != 'finished' && $row['status'] != 'failed' && $row['status'] != 'queued') {
      $query = "update ifoldrna_tasks set status='stopping' where id='$id'";

      $conn->exec($query);
    }

    header("HTTP/1.1 200 OK");
    echo "Stopped successfully";
  } else {
    header("HTTP/1.1 404 No Permissions");
    echo "No permissions";
  }
});

