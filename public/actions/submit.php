<?php

require_once(__DIR__.'/utils.php');
require_once(__DIR__.'/check-input.php');

user_do(function ($conn) {

    $id = rand(1,999999999);

    $userid = $_SESSION['user']['id'];

    $title = '';
//    $file_names = [];
//    $file_contents = [];
    $files = [];
    foreach ($_FILES as $key => $val) {
//      echo $key."\n";
      $content = file_get_contents($val['tmp_name']);
//      $content_quote = $conn->quote($content);

//      array_push($file_names, $key);
//      array_push($file_contents, $content_quote);

      $files[$key] = $content;

      if ($key == 'par') {
        $title = json_decode($content, true)['title'];
      }
    }

    $errors = checkInput($files);

    $num_errors = count($errors);

    if ($num_errors == 0) {
        $files_quote = $conn->quote(json_encode($files));
        $query = sprintf("insert into ifoldrna_tasks (tsubmit, id, userid, title, %s) values(CURRENT_TIMESTAMP(), '$id', '$userid', '$title', %s)", 'files', $files_quote);
        $conn->exec($query);
    }

    echo json_encode($errors);


});

