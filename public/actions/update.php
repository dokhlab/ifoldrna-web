<?php

require_once(__DIR__.'/utils.php');

admin_do(function ($conn) {
  $return = [];

  $content = json_decode(file_get_contents($_FILES['file']['tmp_name']));

  $conds = [];

  foreach ($content as $k => $v) {
    if ($k == '__table') {
      $table = $v;
    } elseif ($k == '__key') {
      $key = $v;
    }
  }

  foreach ($content as $k => $v) {
    if ($k != '__table' && $k != '__key') {
      if ($k == 'password' || $k == 'passwd') {
        $v = md5($v);
      }

      $return[$k] = $v;

      $q = $conn->quote($v);
      if ($k == $key) {
        $value = $q;
      } else {
        $conds[] = "$k=$q";
      }
    }
  }
  $condition = implode(", ", $conds);
  $query = "update $table set $condition where $key=$value";
//  echo $query;
  $conn->exec($query);

  echo json_encode($return);
});

