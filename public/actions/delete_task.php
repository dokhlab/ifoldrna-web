<?php

require_once(__DIR__.'/utils.php');

user_do(function ($conn) {
  $id = $_GET['id'];
  $userid = $_SESSION['user']['id'];
  $userlevel = $_SESSION['user']['level'];

  $query = "select id, userid from ifoldrna_tasks where id='$id'";
  $stmt = $conn->prepare($query); 
  $stmt->execute(); 
  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  if ($userid == $row['userid'] || $userlevel == '10') {
    $query = "delete from ifoldrna_tasks where id='$id'";
    $conn->exec($query);
    header("HTTP/1.1 200 OK");
    echo "Delete successfully";
  } else {
    header("HTTP/1.1 404 No Permissions");
    echo "No permissions!";
  }
});

