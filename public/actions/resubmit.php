<?php

require_once(__DIR__.'/utils.php');

user_do(function ($conn) {
  $id = $_POST['id'];

  $userid = $_SESSION['user']['id'];
  $userlevel = $_SESSION['user']['level'];

  $query = "select id, userid from ifoldrna_tasks where id='$id'";
  $stmt = $conn->prepare($query); 
  $stmt->execute(); 
  $row = $stmt->fetch(PDO::FETCH_ASSOC);


  if ($userid == $row['userid'] || $userlevel == '10') {
    // Set input
#    $input = [];
#    foreach ($_POST as $k => $v) {
#      if ($k != 'id') {
#        $input[$k] = $conn->quote($v);
#      }
#    }

#    $query = "update ifoldrna_tasks set status='queued', ".implode(', ', array_map(function ($i) use ($input) { $v=$input[$i]; return "$i=$v"; }, array_keys($input)))." where id='$id'";

    $file_par = file_get_contents($_FILES['par']['tmp_name']);

    $task_id = json_decode($file_par, true)['id'];
    $task_par = json_decode($file_par, true)['par'];

    $parQuote = $conn->quote(json_encode($task_par));

    $query = "update ifoldrna_tasks set status='queued', par=$parQuote where id='$task_id'";

    $conn->exec($query);

    header("HTTP/1.1 200 OK");
    echo "Resubmitted successfully";
  } else {
    header("HTTP/1.1 404 No Permissions");
    echo "No permissions";
  }
});

