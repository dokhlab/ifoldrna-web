<?php

require_once('utils.php');

anyone_do(function ($conn) {
  $daemon_path = __DIR__."/../../daemon";

  $len = $_GET['len'];
  $rmsd = $_GET['rmsd'];

  $cmd = "$daemon_path/RNA_PredictionSignificance/RNA_PredictionSignificance.linux $len $rmsd";
  $str = shell_exec($cmd);

  echo $str;
});

