<?php

require_once(__DIR__.'/utils.php');

init_head();

$id = $_GET['id'];
$cluster = $_GET['cluster'];
$log = $_GET['log'];
if (isset($cluster)) {
  $file = "$id/dmd.sim/cluster.$cluster.pdb";
  $url = $config['download']['prefix']."/$file";
  $data = file_get_contents($url);
} else if (isset($log)) {
  $file = "$id/log.txt";
  $url = $config['download']['prefix']."/$file";
  $data = file_get_contents($url);
} else {
  $file = "$id/dmd.sim/clusters.tar.gz";
  $url = $config['download']['prefix']."/$file";
  $data = file_get_contents($url);
}

header('Content-Description: File Transfer');

$ext = pathinfo($name, PATHINFO_EXTENSION);
if ($ext == "pdf") {
    header('Content-Type: application/pdf');
    header('Content-Disposition: inline; filename="'.basename($file).'"');
} else {
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="'.basename($file).'"');
}

header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . strlen($data));

echo $data;

