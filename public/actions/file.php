<?php

require_once(__DIR__.'/utils.php');

anyone_do(function ($conn) {
#  global $ftpHost, $ftpUser, $ftpPass;
  global $fileHost;

  $path = $_GET['path'];
  $name = basename($path);
  $format = pathinfo($name, PATHINFO_EXTENSION);

  if (empty($format)) {
    echo file_get_contents("http://$fileHost/download/$path");
  }
  else {
    header($_SERVER["SERVER_PROTOCOL"]." 200 OK");
    header("Cache-Control: public"); // needed for internet explorer
    header("Content-Type: application/octet-stream");
    header("Accept-Ranges: bytes");
    header("Content-Disposition: attachment; filename=$name");
#    echo file_get_contents("ftp://$ftpHost/$path");
    echo file_get_contents("http://$fileHost/download/$path");
  }
});
