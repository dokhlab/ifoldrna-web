<?php

require_once(__DIR__.'/utils.php');

user_do(function ($conn) {
    $userid = $_SESSION['user']['id'];
    $userlevel = intval($_SESSION['user']['level']);
    $query = "";

    if ($userlevel == 10) {
      $query = "select ifoldrna_tasks.id, title, users.username, userid, tsubmit, tprocess, tfinish, status, results from ifoldrna_tasks left join users on ifoldrna_tasks.userid=users.id order by tsubmit desc";
    } else {
      $query = "select ifoldrna_tasks.id, title, users.username, userid, tsubmit, tprocess, tfinish, status, results from ifoldrna_tasks left join users on ifoldrna_tasks.userid=users.id where ifoldrna_tasks.userid='$userid' order by tsubmit desc";
    }

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $row = $stmt->fetchall(PDO::FETCH_ASSOC);
    $result = ['tasks' => $row];
    print_r(json_encode($result));
});

