<?php

function checkInput ($files) {
  $results = [];
  $par = json_decode($files['par'], true);
  $taskType = $par['taskType'];

  $seq = $par['seq'];
  $ss = $par['ss'];

  if (isset($seq) && isset($ss)) {
//    $results[] = "Par: $par";
//    $results[] = "Seq: $seq";
//    $results[] = "SS: $ss";

    $n_seq = strlen($seq);
    $n_ss = strlen($ss);

    if ($n_seq == 0) {
      $results[] = "Sequence is empty!";
    }

    if ($n_seq != 0) {
      if (!preg_match('/[AUGC]+/', $seq)) {
        $results[] = "Sequence should only contains 'A', 'U', 'G', or 'C' (Lowercase is not accepted).";
      }
    }

//    if ($n_ss == 0) {
//      $results[] = "Secondary structure is empty!";
//    }

    if ($n_seq != 0 && $n_ss != 0) {
      if ($n_seq != $n_ss) {
        $results[] = "Length of the sequence ($n_seq) and the secondary structure ($n_ss) should be equal!";
      }
    }

    if ($n_ss != 0) {
      if (!preg_match('/[()\[\].]+/', $ss)) {
        $results[] = "Secondadry structure should only contains '.', '(', ')', '[', or ']'!";
      }

      // check the parentheses and brackets
      $left_paren = array();
      $left_bracket = array();
      $err_nuc = array();
      for ($i = 0; $i < strlen($ss); $i++) {
        if ($ss[$i] == '(') {
          array_push($left_paren, $i);
        } else if ($ss[$i] == '[') {
          array_push($left_bracket, $i);
        } else if ($ss[$i] == ')') {
          if (count($left_paren) == 0) {
            array_push($err_nuc, $i);
          } else {
            array_pop($left_paren);
          }
        } else if ($ss[$i] == ']') {
          if (count($left_bracket) == 0) {
            array_push($err_nuc, $i);
          } else {
            array_pop($left_bracket);
          }
        }
      }
      $err_nuc = array_merge($err_nuc, $left_paren, $left_bracket);
      if (count($err_nuc) != 0) {
        $err_inf = "Please check your 2D structure: ";
        for ($i = 0; $i < strlen($ss); $i++) {
          if (in_array($i, $err_nuc)) {
            $err_inf = $err_inf."<font color='#FF0000'>".$ss[$i]."</font>";
          } else {
            $err_inf = $err_inf.$ss[$i];
          }
        }
        $err_inf = $err_inf."<br />";
        $results[] = $err_inf;
      }
    }
  }

  $pdb = $par['pdb'];
  if (isset($pdb)) {
    if (strlen($pdb) == 0) {
      $results[] = "PDB file is empty!";
    }
  }

  $hrp = $files['hrp'];
  if (isset($hrp)) {
    if (isset($seq)) {
      $n_seq = strlen($seq);
      $lines = explode("\n", $hrp);
      $nline = 1;
      foreach ($lines as $k => $v) {
        $v = trim($v);
        $line = explode(" ", $v);
        if (count($line) == 2) {
          $n = intval($line[0]);
          if ($n > $n_seq) {
            $results[] = "Out-of-range index in HRP file detected (Line $nline: $v)!";
          }
        }
        $nline += 1;
      }
    } else {
      $results[] = 'Please provide the sequence!';
    }
  }

  return $results;
}
