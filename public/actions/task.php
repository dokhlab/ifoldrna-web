<?php

require_once(__DIR__.'/utils.php');

user_do(function ($conn) {
    $id = $_GET['id'];

    $query = "select * from ifoldrna_tasks where id=$id";

    $stmt = $conn->prepare($query); 
    $stmt->execute(); 
    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    print_r(json_encode($row));
});
