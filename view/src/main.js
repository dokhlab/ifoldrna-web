// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import vueConfig from 'vue-config'
import backConfig from '../../config.json'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import axios from 'axios'

const configs = {
  HOST: backConfig.host
}

Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(Vuex)
Vue.use(vueConfig, configs)

const store = new Vuex.Store({
  state: {
    user: '',

    view: 'fullscreen',

    frontConfig: {
      layout: {
        align: 'left'
      },

      timeline: {
        src: '',
        width: '',
        height: ''
      },

      home: {
        src: '',
        edit: ''
      },

      research: {
        src: '',
        edit: ''
      },

      tools: {
        src: '',
        edit: ''
      }
    }
  },

  mutations: {
    autoLogin (state, cb = () => {}) {
      axios({
        method: 'get',
        url: configs.HOST + '/dokhlab/actions/user.php',
        withCredentials: true
      }).then(response => {
        var r = response.data
        if (r.status === 1) {
          state.user = r.user
          cb()
        }
      }).catch(error => {
        alert(error.response.headers)
      })
    },

    signUp (state, par) {
      let formData = new FormData()
      for (let i in par.user) {
        formData.append(i, par.user[i])
      }

      axios({
        method: 'post',
        url: configs.HOST + '/dokhlab/actions/sign_up.php',
        data: formData,
        config: {headers: {'Content-Type': 'multipart/form-data'}},
        withCredentials: true
      }).then(response => {
        var r = response.data
        if (r.status === 1) {
          alert('Congratulations! Your account is set up successfully!')
          state.user = r.user
          if ('cb' in par) {
            par.cb()
          }
        } else {
          alert('Sign-up failed! ' + r.msg)
        }
      }).catch(() => {
        alert('Sign-up failed!')
      })
    },

    login (state, par) {
      let formData = new FormData()
      formData.append('username', par.username)
      formData.append('password', par.password)

      axios({
        method: 'post',
        url: configs.HOST + '/dokhlab/actions/login.php',
        data: formData,
        config: {headers: {'Content-Type': 'multipart/form-data'}},
        withCredentials: true
      }).then(response => {
        var r = response.data
        if (r.status === 1) {
          state.user = r.user
          if ('cb' in par) {
            par.cb()
          }
        } else {
          alert('Login failed! ' + r.msg)
        }
      }).catch(() => {
        alert('Login failed!')
      })
    },

    logout (state) {
      axios({
        method: 'get',
        url: configs.HOST + '/dokhlab/actions/logout.php',
        withCredentials: true
      }).then(response => {
        state.user = ''
        console.log('Logout...')
      }).catch(() => {
        alert('Logout failed!')
      })
    },

    viewFullscreen (state) {
      state.view = 'fullscreen'
    },

    viewWindow (state) {
      state.view = 'window'
    },

    initFrontConfig (state, conf) {
      state.frontConfig = conf
    },

    updateFrontConfig (state, conf) {
      let parts = [new Blob([JSON.stringify(conf)], { type: 'application/json' })]
      let fileToSend = new File(parts, 'front-config.json', {
        type: 'application/json'
      })

      let formData = new FormData()

//      formData.append('config', JSON.stringify(conf))
      formData.append('config', fileToSend)

      let oldConf = state.frontConfig
      state.frontConfig = conf

      axios({
        method: 'post',
        url: configs.HOST + `/dokhlab/actions/front-config/update.php`,
        data: formData,
        config: {headers: {'Content-Type': 'multipart/form-data'}},
        withCredentials: true
      }).then(response => {
        console.log(response)
      }).catch(error => {
        alert('Update website config failed!!!')
        state.frontConfig = oldConf
        console.log(error.response)
      })
    }
  }
})

const requireComponent = require.context(
  // The relative path of the components folder
  './components/base',
  // Whether or not to look in subfolders
  false,
  // The regular expression used to match base component filenames
  /[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)

  // Get PascalCase name of component
  const componentName = upperFirst(
    camelCase(
      // Strip the leading `./` and extension from the filename
      fileName.replace(/^\.\/(.*)\.\w+$/, '$1')
    )
  )

  // Register component globally
  Vue.component(
    componentName,
    // Look for the component options on `.default`, which will
    // exist if the component was exported with `export default`,
    // otherwise fall back to module's root.
    componentConfig.default || componentConfig
  )
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  render: h => h(App)
})
