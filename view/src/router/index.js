import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Profile from '@/components/Profile'
import Help from '@/components/Help'
import Users from '@/components/Users'
import Submit from '@/components/Submit'
import Search from '@/components/Search'
import Tools from '@/components/Tools'
import Publications from '@/components/Publications'
import Members from '@/components/Members'
import Management from '@/components/Management'
import Events from '@/components/Events'
import NewTask from '@/components/NewTask'
import TaskManagement from '@/components/TaskManagement'
import Task from '@/components/Task'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index'
    },

    {
      path: '/home',
      name: 'Home',
      component: Home
    },

    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },

    {
      path: '/help',
      name: 'Help',
      component: Help
    },

    {
      path: '/users',
      name: 'Users',
      component: Users
    },

    {
      path: '/submit',
      name: 'Submit',
      component: Submit
    },

    {
      path: '/search',
      name: 'Search',
      component: Search
    },

    {
      path: '/tools',
      name: 'Tools',
      component: Tools
    },

    {
      path: '/publications',
      name: 'Publications',
      component: Publications
    },

    {
      path: '/members',
      name: 'Members',
      component: Members
    },

    {
      path: '/management',
      name: 'Management',
      component: Management
    },

    {
      path: '/newtask',
      name: 'NewTask',
      component: NewTask
    },

    {
      path: '/events',
      name: 'Events',
      component: Events
    },

    {
      path: '/queue',
      name: 'TaskManagement',
      component: TaskManagement
    },

    {
      path: '/task/:id',
      name: 'Task',
      component: Task
    }
  ]
})
